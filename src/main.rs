mod cli;

use structopt::StructOpt;
use std::ffi::OsStr;
use std::io;
use std::fs;
use std::path::{Path, PathBuf};
use std::process;
use std::env;

#[derive(Debug)]
struct Wine {
    prefix: PathBuf,
    wine: PathBuf,
}

impl Wine {
    pub fn new<P: AsRef<Path>>(prefix: P, wine: P) -> Self {
        let (prefix, wine) = (prefix.as_ref(), wine.as_ref());
        if !prefix.starts_with("/") { panic!("ERROR! WINEPREFIX must be absolute!"); }
        if !prefix.exists() { panic!("ERROR! WINEPREFIX doesn't exist!"); }
        if !wine.exists() { panic!("ERROR! wine executable doesn't exist!") }
        Self {
            prefix: prefix.to_owned(),
            wine: wine.to_owned(),
        }
    }

    pub fn spawn<P: AsRef<OsStr>, S: AsRef<OsStr>, I: IntoIterator<Item = S>>(
        &self,
        program: P,
        args: I,
    ) -> Result<process::Child, io::Error> {
        process::Command::new(&self.wine)
            // shut up
            .stdout(process::Stdio::null())
            .stderr(process::Stdio::null())
            .arg(program)
            .args(args)
            .env("WINEPREFIX", &self.prefix)
            .spawn()
    }

    pub fn roblox(&self) -> Option<PathBuf> {
        let mut path = self.prefix.clone();
        path.push(&format!(
            "drive_c/users/{}/AppData/Local/Roblox/Versions",
            env::var("USER").ok()?
        ));
        for entry in path.read_dir().ok()? {
            let entry = entry.ok()?;
            if entry.metadata().ok()?.is_dir() {
                path.push(entry.path());
                path.push("RobloxPlayerLauncher.exe");
                return Some(path);
            }
        }
        None
    }

    pub fn install_roblox(&self) -> Option<()> {
        let response = ureq::get("https://roblox.com/download/client")
            .call().
            ok()?;
        if response.status() != 200 {
            None
        } else {
            let installer = "/tmp/rblx_rbxinstaller";
            let mut f = fs::File::create(installer).ok()?;
            let mut reader = response.into_reader();
            io::copy(&mut reader, &mut f).ok()?;
            let exit = self.spawn::<_, &'static str, _>(installer, []).ok()?.wait().ok()?;
            fs::remove_file(installer).ok()?;
            if exit.success() { Some(()) } else { None }
        }
    }
}

fn main() {
    let cfg = cli::Program::from_args();
    let wine = Wine::new(&cfg.prefix, &cfg.wine);
    match cfg.command {
        cli::Command::Player { launch_token: None, save } if save => {
            let exe = env::current_exe().unwrap();
            let desktop = format!(r#"[Desktop Entry]
Version=1.0
Type=Application
Name=Roblox Player
NoDisplay=false
Comment=Roblox Experience Player
Exec={} --wine {} --prefix {} player "%u"
MimeType=x-scheme-handler/roblox-player;"#, exe.display(), cfg.wine.display(), cfg.prefix.display());
            println!("{}", desktop);
        }
        cli::Command::Player { launch_token: Some(launch_token), save } => {
            if save {
                eprintln!("WARN! save flag being ignored");
            }
            let roblox = wine.roblox();
            match roblox {
                Some(roblox) => {
                    wine.spawn(roblox, &[launch_token]).unwrap();
                },
                None => {
                    panic!("ERROR! Roblox was not found, does it need to be installed?");
                }
            }
        },
        cli::Command::Player { launch_token: None, save: _ } => { panic!("ERROR! Did not recieve launch token") },
        cli::Command::Install => {
            wine.install_roblox().unwrap_or_else(|| panic!("ERROR! An error occurred while installing roblox"));
            println!("STATUS! Roblox was installed successfully")
        },
    }
}

// https://gitlab.com/brinkervii/grapejuice/-/blob/master/src/grapejuice_common/wine/wineprefix_roblox.py