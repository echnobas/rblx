use std::path::PathBuf;
use std::ffi::OsString;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(
about = "roblox launcher helper; roblox must be installed before registering desktop file",
setting = structopt::clap::AppSettings::DisableHelpSubcommand)]
pub struct Program {
    #[structopt(short = "w", long = "wine")]
    pub wine: PathBuf,
    #[structopt(short = "p", long = "prefix")]
    pub prefix: PathBuf,
    #[structopt(subcommand)]
    pub command: Command,
}

#[derive(StructOpt, Debug)]
pub enum Command {
    Player {
        launch_token: Option<OsString>,
        #[structopt(short = "s", long = "save", help = "output desktop file, only recognised if no launch token passed")]
        save: bool,
    },
    Install,
}